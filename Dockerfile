FROM nginx
RUN rm /etc/nginx/conf.d/default.conf
COPY build /webcontent
COPY nginx-config /etc/nginx/conf.d