import React, {useState, useEffect, useMemo} from 'react';
import {observer} from 'mobx-react-lite';
import {Route, Switch} from 'react-router-dom';
import {CssBaseline, Backdrop, CircularProgress} from '@material-ui/core';
import {makeStyles, ThemeProvider, createMuiTheme} from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import auth from './services/auth';
import Header from './components/Header';
import Sidebar from './components/Sidebar';
import {PrivateRoute, PrivateContainer} from './components/PrivateRoute';
import Login from './components/Login';
import Workspace from './components/Workspace';
import WorkspaceCompleted from './components/WorkspaceCompleted';
import {ThemeContext, themes} from './utils/theme';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex'
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    position: 'relative',
    overflowY: 'auto',
    height: `calc(100vh - 64px)`,
    [theme.breakpoints.down('xs')]: {
      paddingLeft: '56px',
    },
  },
  loading: {
    backgroundColor: '#fff'
  }
}));

const initialSidebarState = localStorage.getItem('sidebarOpened') === 'true';
const initialTheme = localStorage.getItem('theme') || themes.dark;

const App = observer(() => {
  const classes = useStyles();
  const [sidebarOpened, setSidebarOpened] = useState(initialSidebarState);
  const [preferredTheme, setPreferredTheme] = useState(initialTheme);

  //todo: add checkbox to use auto preferred theme
  //const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');
  const theme = useMemo(() => createMuiTheme({
    palette: {
      type: preferredTheme,
    },
  }), [preferredTheme]);

  const isMobile = useMediaQuery(theme.breakpoints.down('sm'));

  useEffect(() => {
    if (isMobile) setSidebarOpened(false);
  }, [isMobile]);

  useEffect(() => {
    auth.init();
  }, []);

  const toggleSidebar = () => {
    const newSidebarState = !sidebarOpened;
    localStorage.setItem('sidebarOpened', String(newSidebarState));
    setSidebarOpened(newSidebarState);
  };

  const toggleTheme = () => {
    const newTheme = preferredTheme === themes.light ? themes.dark : themes.light;
    localStorage.setItem('theme', newTheme);
    setPreferredTheme(newTheme);
  };

  return (!auth.isInit) ? (
    <Backdrop className={classes.loading} open={true}>
      <CircularProgress color='primary'/>
    </Backdrop>
  ) : (
    <ThemeContext.Provider value={{theme: preferredTheme, toggleTheme}}>
      <ThemeProvider theme={theme}>
        <CssBaseline/>
        <PrivateContainer>
          <Header sidebarToggleHandler={toggleSidebar}/>
          <div className={classes.appBarSpacer}/>
        </PrivateContainer>
        <div className={classes.root}>
          <PrivateContainer>
            <Sidebar open={sidebarOpened}/>
          </PrivateContainer>
          <Switch>
            <PrivateRoute exact path="/">
              <main className={classes.content}>
                <Workspace/>
              </main>
            </PrivateRoute>
            <PrivateRoute path="/completed">
              <main className={classes.content}>
                <WorkspaceCompleted/>
              </main>
            </PrivateRoute>
            <Route path="/login">
              <Login/>
            </Route>
          </Switch>
        </div>
      </ThemeProvider>
    </ThemeContext.Provider>
  );
});

export default App;
