export class User {
  /**
   * @param {string} name
   * @param {string} email
   * @param {string} avatarUrl
   */
  constructor({name, email, avatarUrl}) {
    this.name = String(name);
    this.email = String(email);
    this.avatarUrl = String(avatarUrl);
  }

  static fromAuth(GoogleUser) {
    const profile = GoogleUser.getBasicProfile();
    return new User({
      name: profile.getName(),
      email: profile.getEmail(),
      avatarUrl: profile.getImageUrl()
    });
  }
}