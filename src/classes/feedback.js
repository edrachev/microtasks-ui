import React from 'react';

export const FeedbackTypes = {
  ERROR: 'ERROR',
  INFO: 'INFO',
  WARNING: 'WARNING',
  SUCCESS: 'SUCCESS'
};

export class Feedback {
  /**
   * @param {FeedbackTypes} [type]
   * @param {string} component
   * @param {string} [description]
   * @param {object} [original]
   */
  constructor({type, component, description, original}, isLog) {
    this.type = String(type ?? FeedbackTypes.ERROR);
    this.component = String(component);
    this.description = String(description ?? '');
    this.original = original;
    this.key = Date.now() + Math.random();
    if (isLog) {
      console[this.type === FeedbackTypes.ERROR ? 'error' : 'warn'](this.toString());
      console.dir(this.original);
    }
  }

  toString() {
    return `${this.type}: [${this.component}] ${this.description}`;
  }

  toReact() {
    return (
      <>
        <b>{this.component}:&nbsp;</b> {this.description}
      </>
    );
  }
}
