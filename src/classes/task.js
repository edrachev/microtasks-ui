/**
 * @readonly
 * @enum {string}
 */
export const TaskTypes = {
  TODAY: 'TODAY',
  SOON: 'SOON',
  LATER: 'LATER'
};

export class Task {
  /**
   * @param {string} [uuid]
   * @param {string} summary
   * @param {TaskTypes} [category]
   * @param {number} [order] - Task order inside a category, 0 is highest
   */
  constructor({uuid, summary, category, order}) {
    this.uuid = String(uuid ?? String(Date.now() + Math.random()));
    this.summary = String(summary);
    this.category = category;
    this.order = Number(order ?? 0);
  }
}