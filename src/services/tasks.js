import {makeAutoObservable, runInAction} from 'mobx'
// import debounce from 'debounce';
import auth from './auth';
import {Task, TaskTypes} from '../classes/task';
import config from '../config.json';
import {Feedback} from '../classes/feedback';
import feedbacks from '../services/feedbacks';

class Tasks {
  workspace = {}
  stopAnimation = false

  constructor() {
    Object.keys(TaskTypes).forEach(key => {
      this.workspace[key] = []
    });
    makeAutoObservable(this);
  }

  fetch() {
    return fetch(`${config.baseUrl}/api/v1/workspace`, {
      method: 'GET',
      withCredentials: true,
      headers: {
        'Authorization': `Bearer ${auth.accessToken}`
      }
    }).then(response => response.json())
      .then(workspace => {
        runInAction(() => {
          workspace.forEach(item => {
            this.workspace[item.category] = item.tasks.map((task, index) => {
              return new Task({
                uuid: task.uuid,
                summary: task.summary,
                category: item.category,
                order: index
              })
            });
          });
        });
      }).catch(e => {
        return feedbacks.add(new Feedback({
          component: 'Tasks',
          description: 'Fetch Workspace Error',
          original: e
        }, true));
      });
  }

  complete(task) {
    return fetch(`${config.baseUrl}/api/v1/workspace/complete-task`, {
      method: 'POST',
      withCredentials: true,
      body: JSON.stringify({
        uuid: task.uuid
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${auth.accessToken}`
      }
    }).then(() => runInAction(() => {
      this.workspace[task.category] = this.workspace[task.category].filter(tsk => tsk.uuid !== task.uuid);
    })).catch(e => {
      return feedbacks.add(new Feedback({
        component: 'Tasks',
        description: 'Complete Error',
        original: e
      }, true));
    });
  }

  add(task) {
    let tasks = this.workspace[task.category];

    return fetch(`${config.baseUrl}/api/v1/workspace/create-task`, {
      method: 'POST',
      withCredentials: true,
      body: JSON.stringify({
        summary: task.summary,
        category: task.category
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${auth.accessToken}`
      }
    }).then(response => response.json())
      .then((data) => runInAction(() => {
        //update orders
        tasks.forEach(tsk => {
          if (tsk.order >= task.order) {
            tsk.order++;
          }
        });

        task.uuid = data.uuid;
        tasks.push(task);
        this.workspace[task.category] = [...tasks];
      })).catch(e => {
        return feedbacks.add(new Feedback({
          component: 'Tasks',
          description: 'Add Error',
          original: e
        }, true));
      });
  }

  remove(task) {
    let tasks = this.workspace[task.category];
    //update orders
    tasks.forEach(tsk => {
      if (tsk.order >= task.order) {
        tsk.order--;
      }
    });

    this.workspace[task.category] = tasks.filter(tsk => tsk.uuid !== task.uuid);

    //todo: add history service

    return fetch(`${config.baseUrl}/api/v1/workspace/task/${task.uuid}`, {
      method: 'DELETE',
      withCredentials: true,
      headers: {
        'Authorization': `Bearer ${auth.accessToken}`
      }
    }).catch(e => {
      return feedbacks.add(new Feedback({
        component: 'Tasks',
        description: 'Delete Error',
        original: e
      }, true));
    });
  }

  update(task, properties) {
    this.stopAnimation = true;
    const oldOrder = task.order;
    const newOrder = properties.order != null ? properties.order : oldOrder;
    const oldCategory = task.category;
    const newCategory = properties.category != null ? properties.category : oldCategory;
    let oldCategoryTasks = this.workspace[oldCategory];
    //update orders if only order's changed
    if (newOrder !== oldOrder && newCategory === oldCategory) {
      oldCategoryTasks
        .forEach(tsk => {
          if (newOrder > oldOrder && tsk.order <= newOrder && tsk.order >= oldOrder) {
            tsk.order--;
          }
          if (oldOrder > newOrder && tsk.order <= oldOrder && tsk.order >= newOrder) {
            tsk.order++;
          }
        });
      this.workspace[oldCategory] = [...oldCategoryTasks];
    }
    //update orders if category's changed
    if (newCategory !== oldCategory) {
      let newCategoryTasks = this.workspace[newCategory];
      oldCategoryTasks
        .forEach(tsk => {
          if (tsk.order > oldOrder) {
            tsk.order--;
          }
        });
      newCategoryTasks
        .forEach(tsk => {
          if (tsk.order >= newOrder) {
            tsk.order++;
          }
        });
      this.workspace[oldCategory] = oldCategoryTasks.filter(tsk => tsk.uuid !== task.uuid);
      newCategoryTasks.push(task);
      this.workspace[newCategory] = [...newCategoryTasks];
    }
    Object.assign(task, properties);

    return fetch(`${config.baseUrl}/api/v1/workspace/update-task`, {
      method: 'POST',
      withCredentials: true,
      body: JSON.stringify(task),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${auth.accessToken}`
      }
    }).then(() => runInAction(() => {
      this.stopAnimation = false;
    })).catch(e => {
      runInAction(() => {
        this.stopAnimation = false;
      });
      return feedbacks.add(new Feedback({
        component: 'Tasks',
        description: 'Update Error',
        original: e
      }, true));
    })
  }
}

export default new Tasks();