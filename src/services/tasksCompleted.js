import {makeAutoObservable, runInAction} from 'mobx'
import auth from './auth';
import {Task} from '../classes/task';
import config from '../config.json';
import {Feedback} from '../classes/feedback';
import feedbacks from '../services/feedbacks';

class TasksCompleted {
  tasks = []

  constructor() {
    makeAutoObservable(this);
  }

  fetch() {
    return fetch(`${config.baseUrl}/api/v1/workspace/completed-tasks`, {
      method: 'GET',
      withCredentials: true,
      headers: {
        'Authorization': `Bearer ${auth.accessToken}`
      }
    }).then(response => response.json())
      .then(tasks => {
        runInAction(() => {
          this.tasks = tasks.map((task, index) => {
            return new Task({
              uuid: task.uuid,
              summary: task.summary,
              order: index
            })
          });
        });
      }).catch(e => {
        return feedbacks.add(new Feedback({
          component: 'Completed Tasks',
          description: 'Fetch Completed Tasks Error',
          original: e
        }, true));
      });
  }

  uncomplete(task) {
    return fetch(`${config.baseUrl}/api/v1/workspace/uncomplete-task`, {
      method: 'POST',
      withCredentials: true,
      body: JSON.stringify({
        uuid: task.uuid
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${auth.accessToken}`
      }
    }).then(() => runInAction(() => {
      this.tasks = this.tasks.filter(tsk => tsk.uuid !== task.uuid);
    })).catch(e => {
      return feedbacks.add(new Feedback({
        component: 'Completed Tasks',
        description: 'Uncomplete Error',
        original: e
      }, true));
    });
  }

  remove(task) {
    //todo: add history service
    this.tasks = this.tasks.filter(tsk => tsk.uuid !== task.uuid);

    return fetch(`${config.baseUrl}/api/v1/workspace/task/${task.uuid}`, {
      method: 'DELETE',
      withCredentials: true,
      headers: {
        'Authorization': `Bearer ${auth.accessToken}`
      }
    }).catch(e => {
      return feedbacks.add(new Feedback({
        component: 'Completed Tasks',
        description: 'Delete Error',
        original: e
      }, true));
    });
  }
}

export default new TasksCompleted();