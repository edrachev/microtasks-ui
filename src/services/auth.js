import {makeAutoObservable, runInAction} from 'mobx'
import config from '../config.json';
import {User} from '../classes/user';
import {Feedback} from '../classes/feedback';
import feedbacks from './feedbacks';

let GoogleAuth;

class Auth {
  user = null
  accessToken = null
  isInit = false
  isLoading = false

  constructor() {
    makeAutoObservable(this);
  }

  fetchToken(idToken) {
    return fetch(`${config.baseUrl}/api/v1/users/authenticate`, {
      method: 'POST',
      withCredentials: true,
      body: JSON.stringify({idToken}),
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => response.json())
      .then(data => {
        runInAction(() => {
          this.accessToken = data.access_token;
          this.user = User.fromAuth(GoogleAuth.currentUser.get());
        });
      });
  }

  init() {
    this.isLoading = true;
    return new Promise((resolve, reject) => {
      window.gapi.load('auth2', () => {
        window.gapi.auth2.init({
          client_id: `${config.googleAppKey}.apps.googleusercontent.com`,
        }).then(() => {
          GoogleAuth = window.gapi.auth2.getAuthInstance();
          const currentUser = GoogleAuth.currentUser.get();
          if (currentUser.isSignedIn()) {
            this.fetchToken(currentUser.getAuthResponse().id_token).then(resolve, reject);
          } else {
            resolve();
          }
        }, reject);
      });
    }).then(() => {
      runInAction(() => {
        this.isInit = true;
      });
    }).catch((e) => {
      return feedbacks.add(new Feedback({
        component: 'Google Auth',
        description: 'Init Error',
        original: e
      }, true));
    }).then(()  => {
      runInAction(() => {
        this.isLoading = false;
      });
    });
  }

  login() {
    this.isLoading = true;
    return GoogleAuth.signIn().then(GoogleUser => {
      return this.fetchToken(GoogleUser.getAuthResponse().id_token);
    }).catch((e) => {
      return feedbacks.add(new Feedback({
        component: 'Google Auth',
        description: 'Login Error',
        original: e
      }, true));
    }).then(()  => {
      runInAction(() => {this.isLoading = false});
    });
  }

  logout() {
    this.isLoading = true;
    return GoogleAuth.signOut().then(() => {
      runInAction(() => {
        this.accessToken = null;
        this.user = null;
        this.isLoading = false;
      });
    });
  }
}

export default new Auth();