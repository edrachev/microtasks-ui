import {makeAutoObservable} from 'mobx';

class Feedbacks {
  list = []

  constructor() {
    makeAutoObservable(this);
  }

  add(feedback) {
    this.list.push(feedback);
    this.list = [...this.list];
    return feedback;
  }

  remove(key) {
    this.list = this.list.filter(fdbk => fdbk.key !== key);
  }

  clear() {
    this.list = [];
  }
}

export default new Feedbacks();