import * as serviceWorkerRegistration from './serviceWorkerRegistration';
import React from 'react';
import reportWebVitals from './reportWebVitals';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router} from 'react-router-dom';
import {SnackbarProvider} from 'notistack';
import App from './App';
import FeedbackBar from './components/FeedbackBar';

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <App />
    </Router>
    <SnackbarProvider maxSnack={5}>
      <FeedbackBar/>
    </SnackbarProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://cra.link/PWA
serviceWorkerRegistration.register();

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals(console.log);