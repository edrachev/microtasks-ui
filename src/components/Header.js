import React from 'react';
import {observer} from 'mobx-react-lite';
import {AppBar, IconButton, Toolbar, Typography} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import {makeStyles} from '@material-ui/core/styles';
import TopMenu from './TopMenu';

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: 'fixed',
    backgroundColor: theme.palette.type === 'light' ? '#f5f5f5' : '#424242',
  },
  menuButton: {
    marginRight: 20,
  },
  title: {
    flexGrow: 1,
  },
  button: {
    margin: theme.spacing(1, 1.5),
  },
}));

const Header = observer(({sidebarToggleHandler}) => {
  const classes = useStyles();

  return (
    <AppBar position="absolute" color="default" elevation={2} className={classes.appBar}>
      <Toolbar>
        <IconButton
          edge="start"
          color="inherit"
          aria-label="open drawer"
          onClick={sidebarToggleHandler}
          className={classes.menuButton}
        >
          <MenuIcon/>
        </IconButton>
        <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
          {/*todo: logo here*/}
          Microtasks
        </Typography>
        {/*todo: name of workspace will be centered here*/}
        <TopMenu></TopMenu>
      </Toolbar>
    </AppBar>
  );
});

export default Header;