import {observer} from 'mobx-react-lite';
import {autorun} from 'mobx';
import {useSnackbar} from 'notistack';
import {FeedbackTypes} from '../classes/feedback';
import feedbacks from '../services/feedbacks';

const FeedbackBar = observer(() => {
  const {enqueueSnackbar} = useSnackbar();

  const displayed = [];

  autorun(() => {
    feedbacks.list.forEach((feedback) => {
      if (displayed.includes(feedback.key)) return;
      enqueueSnackbar(feedback.toReact(), {
        variant: getSeverity(feedback.type)
      });
      displayed.push(feedback.key);
      feedbacks.remove(feedback.key);
    });
  });
  return null;
});

const getSeverity = (type) => {
  switch (type) {
    case FeedbackTypes.ERROR:
      return 'error';
    case FeedbackTypes.INFO:
      return 'info';
    case FeedbackTypes.WARNING:
      return 'warning';
    case FeedbackTypes.SUCCESS:
      return 'success';
    default:
      return 'error';
  }
};

export default FeedbackBar;