import React, {useEffect} from 'react';
import {observer} from 'mobx-react-lite';
import {Container} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import {useSpring, animated, useTransition} from 'react-spring';
import tasksCompleted from '../services/tasksCompleted';
import TaskItem from './TaskItem';

const useStyles = makeStyles((theme) => ({
  container: {
    position: 'relative',
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
}));

const AnimatedContainer = animated(Container);

const WorkspaceCompleted = observer(() => {
  const classes = useStyles();

  const styleProps = useSpring({
    from: {left: '-40px', opacity: 0},
    to: {left: '0px', opacity: 1},
  });

  //following needs only for delay array changes
  const transitions = useTransition(tasksCompleted.tasks, task => task.uuid, {
    from: {opacity: 0},
    enter: {opacity: 1},
    leave: {opacity: 0},
  });

  useEffect(() => {
    tasksCompleted.fetch();
  }, []);

  const completeHandler = (task) => {
    tasksCompleted.uncomplete(task);
  };
  const removeHandler = (task) => {
    tasksCompleted.remove(task);
  };

  return (
    <AnimatedContainer maxWidth="lg" className={classes.container} style={styleProps}>
      {transitions.map(({item: task, key}) => (
        <TaskItem
          key={key}
          task={task}
          completeHandler={completeHandler.bind(null, task)}
          removeHandler={removeHandler.bind(null, task)}
        />
      ))}
    </AnimatedContainer>
  );
});

export default WorkspaceCompleted;