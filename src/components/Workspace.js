import React, {useCallback, useEffect, useReducer} from 'react';
import {observer} from 'mobx-react-lite';
import {Container, Collapse, IconButton} from '@material-ui/core';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import {makeStyles} from '@material-ui/core/styles';
import TaskAdd from './TaskAdd';
import {DragDropContext} from 'react-beautiful-dnd';
import {animated, useSpring} from 'react-spring';
import tasks from '../services/tasks';
import {TaskTypes} from '../classes/task';
import TasksListDroppable from './TasksListDroppable';

const useStyles = makeStyles((theme) => ({
  container: {
    position: 'relative',
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  categoryTitle: {
    marginBottom: '.25em',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginRight: '4px',
  },
  splitterButton: {
    transition: 'transform ease 150ms'
  },
  splitterButtonClosed: {
    transform: 'rotate(180deg)'
  },
}));

const AnimatedContainer = animated(Container);

let initialSplitterState = JSON.parse(localStorage.getItem('workspaceSplitterState'));
if (initialSplitterState == null) {
  initialSplitterState = {};
  Object.keys(TaskTypes).forEach(type => initialSplitterState[type] = true);
}

const Workspace = observer(() => {
  const classes = useStyles();
  const [state, dispatch] = useReducer((state, action) => {
    const newState = {...state, [action]: !state[action]};
    localStorage.setItem('workspaceSplitterState', JSON.stringify(newState));
    return newState;
  }, initialSplitterState);

  useEffect(() => {
    tasks.fetch();
  }, []);

  const styleProps = useSpring({
    from: {left: '-40px', opacity: 0},
    to: {left: '0px', opacity: 1},
  });

  const onDragEnd = useCallback(({source, destination}) => {
    if (!destination) return; // dropped outside the workspace
    const task = tasks.workspace[source.droppableId].find(item => item.order === source.index);
    if (task.order === destination.index && task.category === destination.droppableId) return; // nothing's changed
    tasks.update(task, {
      order: destination.index,
      category: destination.droppableId
    });
  }, []);

  return (
    <AnimatedContainer maxWidth="lg" className={classes.container} style={styleProps}>
      <TaskAdd/>
      <DragDropContext onDragEnd={onDragEnd}>
        {Object.keys(TaskTypes).map(type => tasks.workspace[type] != null && (
          <div key={type}>
            <h2 className={classes.categoryTitle}>
              {TaskTypes[type]}
              <IconButton aria-label="collapse" onClick={() => dispatch(type)}>
                <ArrowDropDownIcon fontSize="inherit" className={`${classes.splitterButton} ${!state[type] ? classes.splitterButtonClosed : ''}`}/>
              </IconButton>
            </h2>
            <Collapse in={state[type]}>
              <TasksListDroppable tasks={tasks.workspace[type]} type={type} />
            </Collapse>
          </div>
        )
        )}
      </DragDropContext>
    </AnimatedContainer>
  );
});

export default Workspace;