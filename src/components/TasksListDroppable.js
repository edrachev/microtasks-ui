import React from 'react';
import {Draggable, Droppable} from 'react-beautiful-dnd';
import {makeStyles} from '@material-ui/core/styles';
import {useTransition} from 'react-spring';
import TaskItem from './TaskItem';
import tasksService from '../services/tasks';

const useStyles = makeStyles(theme => {
  return {
    dropPoint: {
      minHeight: '48px',
      background: theme.palette.type === 'light' ? '#f5f5f5' : '#373737'
    },
    dropPointDragOver: {
      background: theme.palette.type === 'light' ? '#e0e2ea' : '#3b3e47'
    }
  };
});

const completeHandler = (task) => {
  tasksService.complete(task);
};

const removeHandler = (task) => {
  tasksService.remove(task);
};

const TasksListDroppable = ({tasks, type}) => {
  const classes = useStyles();

  //following needs only for delay array changes
  const transitions = useTransition(tasks.sort((a, b) => (a.order - b.order)), task => task.uuid, {
    from: {opacity: 0},
    enter: {opacity: 1},
    leave: {opacity: 0},
    immediate: tasksService.stopAnimation
  });

  return (
    <Droppable droppableId={type}>
      {(provided, snapshot) => (
        <div
          className={`${classes.dropPoint} ${snapshot.isDraggingOver ? classes.dropPointDragOver : ''}`}
          ref={provided.innerRef}
        >
          {transitions.map(({item: task, key}) => (
            <Draggable
              key={key}
              draggableId={task.uuid}
              index={task.order}
            >
              {(provided, snapshot) => (
                <>
                  <TaskItem
                    task={task}
                    completeHandler={completeHandler.bind(null, task)}
                    removeHandler={removeHandler.bind(null, task)}
                    props={{
                      ref: provided.innerRef,
                      ...provided.draggableProps,
                      ...provided.dragHandleProps,
                      style: provided.draggableProps.style,
                      elevation: (snapshot.isDragging ? 7 : 1)
                    }}
                  />
                  {provided.placeholder}
                </>
              )}
            </Draggable>
          ))}
          {provided.placeholder}
        </div>
      )}
    </Droppable>
  );
};

//todo: task.uuid + task.order - looks like shit, but I don't know how to do better

export default TasksListDroppable;