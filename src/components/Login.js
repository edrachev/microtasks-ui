import {observer} from 'mobx-react-lite';
import React from 'react';
import {useHistory} from 'react-router-dom';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import {Avatar, Button, Paper, Grid, Typography, SvgIcon, Backdrop, CircularProgress} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import {ReactComponent as GoogleLogo} from '../images/GoogleGLogo.svg';
import splashPic from '../images/splashPicNight.jpg';
import auth from '../services/auth';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  image: {
    backgroundImage: `url(${splashPic})`,
    backgroundRepeat: 'no-repeat',
    backgroundColor: theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(10, 8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  welcome: {
    marginTop: theme.spacing(2)
  },
  button: {
    margin: theme.spacing(3, 0, 2),
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}));

const Login = observer(() => {
  const classes = useStyles();
  const history = useHistory();

  if (auth.user != null) {
    history.push("/");
  }

  const loginGoogleHandler = () => {
    auth.login().then(() => {
      history.push("/");
    });
  };

  return (
    <Grid container component="main" className={classes.root}>
      <Grid item xs={false} sm={4} md={7} className={classes.image}/>
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon/>
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <Typography component="p" variant="body1" className={classes.welcome}>
            Welcome to Microtasks App. Choose a way to log in.
          </Typography>
          <Button
            type="button"
            fullWidth
            variant="outlined"
            size="large"
            color="primary"
            startIcon={
              <SvgIcon component={GoogleLogo} viewBox="0 0 550 550" />
            }
            className={classes.button}
            disabled={!auth.isInit}
            onClick={loginGoogleHandler}
          >
            Enter with Google
          </Button>

        </div>
      </Grid>

      <Backdrop className={classes.backdrop} open={auth.isInit && auth.isLoading}>
        <CircularProgress color="inherit" />
      </Backdrop>
    </Grid>
  );
});

export default Login;