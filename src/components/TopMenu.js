import React, {useState, useRef, useEffect} from 'react';
import {useHistory} from 'react-router-dom';
import {
  IconButton,
  ClickAwayListener,
  Avatar,
  Grid,
  Grow,
  Paper,
  Popper,
  MenuItem,
  MenuList,
  ListItemIcon,
  Typography
} from '@material-ui/core';
import MeetingRoomIcon from '@material-ui/icons/MeetingRoom';
import InvertColorsIcon from '@material-ui/icons/InvertColors';
import {makeStyles} from '@material-ui/core/styles';
import {ThemeContext, themes} from '../utils/theme';
import auth from '../services/auth';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  paper: {
    marginRight: theme.spacing(2),
  },
  gridItem: {
    padding: theme.spacing(4),
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(2)
  },
  username: {
    fontWeight: 'bold'
  }
}));

const TopMenu = () => {
  const classes = useStyles();
  const history = useHistory();
  const [open, setOpen] = useState(false);
  const anchorRef = useRef(null);

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }
    setOpen(false);
  };

  function handleListKeyDown(event) {
    if (event.key === 'Tab') {
      event.preventDefault();
      setOpen(false);
    }
  }

  // return focus to the button when we transitioned from !open -> open
  const prevOpen = useRef(open);
  useEffect(() => {
    if (prevOpen.current === true && open === false) {
      anchorRef.current.focus();
    }
    prevOpen.current = open;
  }, [open]);

  const handleLogout = (event) => {
    handleClose(event);
    auth.logout().then(() => {
      history.push('/login');
    });
  };

  return (
    <div className={classes.root}>
      <IconButton
        ref={anchorRef}
        size="small"
        aria-controls={open ? 'menu-list-grow' : undefined}
        aria-haspopup="true"
        onClick={handleToggle}
      >
        <Avatar src={auth.user.avatarUrl}></Avatar>
      </IconButton>
      <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition disablePortal>
        {({TransitionProps, placement}) => (
          <Grow
            {...TransitionProps}
            style={{transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom'}}
          >
            <Paper elevation={7}>
              <ClickAwayListener onClickAway={handleClose}>
                <Grid container direction="column">
                  <Grid item className={classes.gridItem}>
                    <Typography align="center" variant="body1" className={classes.username}>
                      {auth.user.name}
                    </Typography>
                    <Typography align="center" variant="body2">
                      {auth.user.email}
                    </Typography>
                  </Grid>
                  <Grid item>
                    <MenuList autoFocusItem={open} id="menu-list-grow" onKeyDown={handleListKeyDown}>
                      <ThemeContext.Consumer>
                        {({theme, toggleTheme}) => (
                          <MenuItem onClick={toggleTheme}>
                            <ListItemIcon>
                              <InvertColorsIcon fontSize="small"/>
                            </ListItemIcon>
                            <Typography variant="inherit">
                              Use {theme === themes.light ? themes.dark : themes.light} theme
                            </Typography>
                          </MenuItem>
                        )}
                      </ThemeContext.Consumer>
                      <MenuItem onClick={handleLogout}>
                        <ListItemIcon>
                          <MeetingRoomIcon fontSize="small"/>
                        </ListItemIcon>
                        <Typography variant="inherit">
                          Logout
                        </Typography>
                      </MenuItem>
                    </MenuList>
                  </Grid>
                </Grid>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    </div>
  );
};
//
// <Button
//   color="primary"
//   variant="outlined"
//   className={classes.button}
//   onClick={handleLogout}
// >
//   Logout
// </Button>

export default TopMenu;