import React, {useEffect, useRef} from 'react';
import {observer} from 'mobx-react-lite';
import {makeStyles} from '@material-ui/core/styles';
import {Button, TextField} from '@material-ui/core';
import {Task, TaskTypes} from '../classes/task';
import tasks from '../services/tasks';

const useStyles = makeStyles(theme => ({
  form: {
    display: 'flex',
    alignItems: 'center'
  },
  textarea: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    flex: 'auto'
  },
  addButton: {
    flex: 'none'
  },
}));

const TaskAdd = observer(() => {
  const summaryEl = useRef(null);
  const classes = useStyles();

  useEffect(() => {
    summaryEl.current.focus();
  }, [summaryEl])

  const addTask = () => {
    const summary = summaryEl.current.value;
    if (summary === '' || summary == null) return;
    tasks.add(new Task({summary, category: TaskTypes.TODAY}));
    summaryEl.current.value = '';
    summaryEl.current.focus();
  };

  const submitHandler = (e) => {
    e.preventDefault();
    addTask();
  };

  const keyPressHandler = (e) => {
    if (!e.shiftKey && !e.ctrlKey && e.code === 'Enter') {
      e.preventDefault();
      addTask();
    }
  };

  return (
    <form
      noValidate
      autoComplete='off'
      className={classes.form}
      onSubmit={submitHandler}
    >
      <TextField
        name='summary'
        label="New Task"
        multiline
        variant='outlined'
        size="small"
        className={classes.textarea}
        inputProps={{ref: summaryEl}}
        onKeyPress={keyPressHandler}
      />
      <Button
        type='submit'
        variant='contained'
        color='primary'
        className={classes.addButton}
      >
        Add
      </Button>
    </form>
  );
});

export default TaskAdd;