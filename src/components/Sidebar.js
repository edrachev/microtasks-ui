import React from 'react';
import {Route, Switch, useHistory} from 'react-router-dom';
import clsx from 'clsx';
import {Divider, Drawer, List, ListItem, ListItemIcon, ListItemText} from '@material-ui/core';
import DashboardIcon from '@material-ui/icons/Dashboard';
import {makeStyles} from '@material-ui/core/styles';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    [theme.breakpoints.down('xs')]: {
      position: 'fixed',
      top: '56px',
    },
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  list: {
    paddingLeft: 8,
    paddingRight: 8,
    [theme.breakpoints.down('xs')]: {
      paddingLeft: 0,
      paddingRight: 0,
    },
  }
}));

const Sidebar = ({open}) => {
  const classes = useStyles();
  const history = useHistory();

  const showCompletedHandler = () => {
    history.push("/completed");
  };

  const showDashboard = () => {
    history.push("/");
  };

  return (
    <Drawer
      variant="permanent"
      classes={{
        paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
      }}
      open={open}
    >
      <Divider/>
      <List className={classes.list}>
        <Switch>
          <Route exact path="/">
            <ListItem button onClick={showCompletedHandler}>
              <ListItemIcon>
                <DashboardIcon/>
              </ListItemIcon>
              <ListItemText primary="Completed"/>
            </ListItem>
          </Route>
          <Route path="/completed">
            <ListItem button onClick={showDashboard}>
              <ListItemIcon>
                <DashboardIcon/>
              </ListItemIcon>
              <ListItemText primary="Dashboard"/>
            </ListItem>
          </Route>
        </Switch>
      </List>
      {/*<Divider/>*/}
      {/*<List>Custom workspaces goes here</List>*/}
    </Drawer>
  );
};

export default Sidebar;