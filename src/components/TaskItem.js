import React, {useRef, useState, useEffect} from 'react';
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Button,
  Checkbox,
  FormControlLabel,
  Typography,
  TextField,
  Link
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import mergeRefs from 'react-merge-refs';
import {makeStyles} from '@material-ui/core/styles';
import {animated, useSpring} from 'react-spring';
import {useMeasure} from 'react-use';
import Linkify from 'react-linkify';
import tasks from '../services/tasks';

const useStyles = makeStyles(theme => ({
  checkboxLabel: {
    alignItems: 'baseline'
  },
  expandIcon: {
    alignItems: 'baseline'
  },
  checkbox: {
    padding: '0 9px'
  },
  summary: {
    lineHeight: 1.5
  },
  summaryText: {
    whiteSpace: 'pre-wrap',
    overflowWrap: 'anywhere',
    lineHeight: 1.5,
  },
  link: {
    textDecoration: 'underline',
    '&:hover': {
      textDecoration: 'none'
    },
  },
  closing: {
    pointerEvents: 'none'
  },
  focused: {
    backgroundColor: 'transparent !important'
  }
}));

const AnimatedAccordion = animated(Accordion);

const TaskItem = ({task, completeHandler, removeHandler, props = {}}) => {
  const classes = useStyles();
  const summaryEl = useRef(null);
  const [isError, setError] = useState(false);
  const [isOpened, setOpened] = useState(false);
  const [isVisible, setVisible] = useState(true);
  const [ref, {height: viewHeight}] = useMeasure();
  props.ref = mergeRefs([props.ref, ref]);

  const {height, opacity} = useSpring({
    from: {opacity: 1, height: 0},
    to: [{opacity: isVisible ? 1 : 0}, {height: isVisible ? viewHeight : 0}],
    config: {delay: 300},
  });
  props.style = {...props.style, height: (isVisible ? 'auto' : height), opacity};

  useEffect(() => {
    if (isOpened) summaryEl.current.focus();
  }, [isOpened]);

  const update = () => {
    if (summaryEl.current.value === task.summary) return; //nothing's changed
    const summary = summaryEl.current.value;
    if (summary !== '') {
      tasks.update(task, {
        summary: summaryEl.current.value
      });
    } else {
      summaryEl.current.value = task.summary;
      setError(false);
    }
  };

  const handleChange = () => {
    const summary = summaryEl.current.value;
    setError(summary === '');
  };

  const handleKeyPress = (e) => {
    if (!e.shiftKey && !e.ctrlKey && e.code === 'Enter') {
      e.preventDefault();
      update();
      setOpened(false);
    }
  };

  const handleAccordionClick = () => {
    if (isOpened) summaryEl.current.blur();
    setOpened(!isOpened);
  };

  const handleCheckboxChange = () => {
    completeHandler();
    setVisible(false);
  };

  return (
    <AnimatedAccordion
      elevation={1}
      {...props}
      className={!isVisible ? classes.closing : ''}
      expanded={isOpened}
    >
      <AccordionSummary
        expandIcon={<ExpandMoreIcon className={classes.expandIcon}/>}
        aria-label="Expand"
        classes={{
          focused: classes.focused
        }}
        onClick={handleAccordionClick}
      >
        <FormControlLabel
          aria-label="Mark as done"
          title="Mark as done"
          classes={{
            root: classes.checkboxLabel
          }}
          onClick={(event) => event.stopPropagation()}
          onFocus={(event) => event.stopPropagation()}
          control={<Checkbox
            classes={{
              root: classes.checkbox
            }}
            onChange={handleCheckboxChange}
          />}
        />
        {isOpened ? (
          <TextField
            id="standard-multiline-flexible"
            label=""
            multiline
            required
            fullWidth
            size="small"
            inputProps={{
              ref: summaryEl,
              className: classes.summary
            }}
            defaultValue={task.summary}
            onClick={(event) => event.stopPropagation()}
            onBlur={update}
            onChange={handleChange}
            onKeyPress={handleKeyPress}
            error={isError}
            helperText={isError ? 'Error: empty summary' : null}
          />
        ) : (
          <Typography
            color="textPrimary"
            className={classes.summaryText}
          >
            <Linkify
              componentDecorator={(decoratedHref, decoratedText, key) => (
                <Link
                  className={classes.link}
                  color="inherit"
                  target='_blank'
                  rel='nofollow noopener'
                  href={decoratedHref}
                  key={key}
                  onClick={(e) => e.stopPropagation()}
                >
                  {decoratedText}
                </Link>
              )}
            >
              {task.summary}
            </Linkify>
          </Typography>
        )}
      </AccordionSummary>
      <AccordionDetails>
        <Button
          type='button'
          variant='contained'
          color='primary'
          onClick={() => {
            removeHandler();
            setVisible(false);
          }}
        >
          Remove
        </Button>
        {/*<Typography color="textSecondary">*/}
        {/*  Details of the task might be here...*/}
        {/*</Typography>*/}
      </AccordionDetails>
    </AnimatedAccordion>
  );
};

export default TaskItem;