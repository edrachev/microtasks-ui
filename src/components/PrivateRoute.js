import {observer} from 'mobx-react-lite';
import {Redirect, Route} from 'react-router-dom';
import auth from '../services/auth';
import React from 'react';

const PrivateRoute = observer(({children, ...rest}) => {
  return (
    <Route
      {...rest}
      render={({location}) =>
        auth.user != null ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: {from: location}
            }}
          />
        )
      }
    />
  );
});

const PrivateContainer = observer(({children}) => {
  return (
    <>
      {auth.user != null ? children : null}
    </>
  );
});

export {PrivateRoute, PrivateContainer};